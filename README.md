# linux-sunxi-fex
## Overview

This repository contains FEX files (ancestors of Linux device tree) for ANTRAX Allwinner A20 based hardware

## Structure

Files purposes:

 * antrax_commonbox.fex - common file for all hardware variants
 * antrax_gsmbox.fex.part - GSMBOX specific file
 * antrax_simbox.fex.part - SIMBOX specific file
 * fttool.fex - configuration for test equipment

## Build

To build FEX files [sunxi-tools](https://github.com/linux-sunxi/sunxi-tools) should be available in PATH. To get final FEX files:

```bash
cat antrax_commonbox.fex antrax_gsmbox.fex.part | sed 's/ANTRAX BOX/ANTRAX GSMBOX/g' | fex2bin > antrax_gsmbox.fex.bin
cat antrax_commonbox.fex antrax_simbox.fex.part | sed 's/ANTRAX BOX/ANTRAX SIMBOX/g' | fex2bin > antrax_simbox.fex.bin
cat antrax_commonbox.fex | fex2bin > antrax_commonbox.fex.bin
cat fttool.fex | fex2bin > fttool.fex.bin
```

### Build with GitLab CI

Repository already contains necessary instructions in **.gitlab-ci.yml**. It requires **linux-sunxi-fex** docker image - Ubuntu 14.04 with installed **sunxi-tools** to be available for GitLab runner.
